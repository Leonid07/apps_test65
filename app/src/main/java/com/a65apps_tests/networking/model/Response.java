package com.a65apps_tests.networking.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    @SerializedName("f_name")
    @Expose
    public String fName;
    @SerializedName("l_name")
    @Expose
    public String lName;
    @SerializedName("birthday")
    @Expose
    public String birthday;
    @SerializedName("avatr_url")
    @Expose
    public String avatrUrl;
    @SerializedName("specialty")
    @Expose
    public List<Specialty> specialty;
}
