package com.a65apps_tests;

import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.a65apps_tests.model.User;
import com.a65apps_tests.viewModel.UserViewModel;
import com.bumptech.glide.Glide;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    final String TAG ="myLog";
    private String Users;
    private String ageUser;
    private UserViewModel userViewModel;
    TextView name;
    TextView lastName;
    TextView birthday;
    TextView Age;
    TextView specialty_name;
    ImageView imageView;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public UserInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserInfoFragment newInstance(String param1, String param2) {
        UserInfoFragment fragment = new UserInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        Users = bundle.getString("name");

        userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        userViewModel.getAllUser().observe(getViewLifecycleOwner(), new Observer<List<User>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onChanged(List<User> users) {
                int siz = users.size();

                name = view.findViewById(R.id.name);
                lastName = view.findViewById(R.id.lastName);
                birthday = view.findViewById(R.id.birthday);
                Age = view.findViewById(R.id.Age);
                specialty_name = view.findViewById(R.id.specialty_name);
                imageView = view.findViewById(R.id.image);

                for(int i=0; i<siz; i++){
                    if(Users.equals(users.get(i).getName())){

                        String birthDate =  users.get(i).getBirthday();
                        if(birthDate != null){
                            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                            int age = 0;
                            try {
                                Date date1 = dateFormat.parse(birthDate);
                                Calendar now = Calendar.getInstance();
                                Calendar dob = Calendar.getInstance();
                                dob.setTime(date1);
                                if (dob.after(now)) {
                                    throw new IllegalArgumentException("Can't be born in the future");
                                }
                                int year1 = now.get(Calendar.YEAR);
                                int year2 = dob.get(Calendar.YEAR);
                                age = year1 - year2;
                                int month1 = now.get(Calendar.MONTH);
                                int month2 = dob.get(Calendar.MONTH);
                                if (month2 > month1) {
                                    age--;
                                } else if (month1 == month2) {
                                    int day1 = now.get(Calendar.DAY_OF_MONTH);
                                    int day2 = dob.get(Calendar.DAY_OF_MONTH);
                                    if (day2 > day1) {
                                        age--;
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            ageUser = String.valueOf(age);

                            if(age == 0){
                                ageUser = "-";
                            }
                            Log.d(TAG, String.valueOf(age));
                        }

                        name.setText(users.get(i).getName());
                        lastName.setText(users.get(i).getLastName());
                        birthday.setText(users.get(i).getBirthday());
                        Age.setText(ageUser);
                        specialty_name.setText(users.get(i).getSpecialty_name());

                        Glide
                                .with(getContext())
                                .load(users.get(i).getAvatr_url())
                                .into(imageView);
                    }
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_info, container, false);
    }
}
