package com.a65apps_tests;

import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.a65apps_tests.model.User;
import com.a65apps_tests.viewModel.UserViewModel;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link WorkersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
@RequiresApi(api = Build.VERSION_CODES.N)
public class WorkersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String Specialty;
    private RecyclerView rv_workers;
    private List<RVWorkers> persons;
    private ArrayList<String> name;
    private ArrayList<String> lastName;
    private ArrayList<String> birthday;
    private UserViewModel userViewModel;
    final String TAG ="myLog";
    private String ageUser;
    private int age;

    public WorkersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WorkersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WorkersFragment newInstance(String param1, String param2) {
        WorkersFragment fragment = new WorkersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = this.getArguments();
        Specialty = bundle.getString("name");


        userViewModel = new ViewModelProvider(getActivity()).get(UserViewModel.class);
        userViewModel.getAllUser().observe(getViewLifecycleOwner(), new Observer<List<User>>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onChanged(List<User> users) {
                int siz = users.size();

             name = new ArrayList<>();
             lastName = new ArrayList<>();
             birthday = new ArrayList<>();


                for(int i = 0; i<siz; i++){

                    if(Specialty.equals(users.get(i).getSpecialty_name())){

                        name.add(users.get(i).getName());
                        lastName.add(users.get(i).getLastName());
                        birthday.add(users.get(i).getBirthday());
                    }
                }

                rv_workers=(RecyclerView) view.findViewById(R.id.rv_workers);

                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                rv_workers.setLayoutManager(llm);
                rv_workers.setHasFixedSize(true);



                initializeData();
                initializeAdapter();
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_workers, container, false);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initializeData(){

        persons = new ArrayList<>();

        int siz = name.size();

        String data1;
        String data2;
        String data3;

        for (int i = 0; i < siz; i++) {
            data1 = name.get(i);
            data2 = lastName.get(i);

            String birthDate =  birthday.get(i);
            if(birthDate != null & birthDate != "-") {
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
               age = 0;
                try {
                    Date date1 = dateFormat.parse(birthDate);
                    Calendar now = Calendar.getInstance();
                    Calendar dob = Calendar.getInstance();
                    dob.setTime(date1);
                    if (dob.after(now)) {
                        throw new IllegalArgumentException("Can't be born in the future");
                    }
                    int year1 = now.get(Calendar.YEAR);
                    int year2 = dob.get(Calendar.YEAR);
                    age = year1 - year2;
                    int month1 = now.get(Calendar.MONTH);
                    int month2 = dob.get(Calendar.MONTH);
                    if (month2 > month1) {
                        age--;
                    } else if (month1 == month2) {
                        int day1 = now.get(Calendar.DAY_OF_MONTH);
                        int day2 = dob.get(Calendar.DAY_OF_MONTH);
                        if (day2 > day1) {
                            age--;
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                ageUser = String.valueOf(age);
            }

            if(age == 0){
                ageUser = "-";
            }

            data3 = ageUser;
            persons.add(new RVWorkers(data1, data2, data3));
        }
    }


    private void initializeAdapter(){
        RVAdapterWorkers rvAdapterWorkers = new RVAdapterWorkers(persons);
        rv_workers.setAdapter(rvAdapterWorkers);

        rvAdapterWorkers.liveData.observe(this, users -> {

            Bundle bundle=new Bundle();
            bundle.putString("name", users.name);

            UserInfoFragment userInfoFragment = new UserInfoFragment();
            userInfoFragment.setArguments(bundle);

            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, userInfoFragment)
                    .addToBackStack(null)
                    .commit();
        });
    }
}
