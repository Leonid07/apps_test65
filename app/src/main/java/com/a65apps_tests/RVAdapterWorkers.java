package com.a65apps_tests;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public final class RVAdapterWorkers extends RecyclerView.Adapter<com.a65apps_tests.RVAdapterWorkers.PersonViewHolder> {
        MutableLiveData<RVWorkers> liveData = new MutableLiveData<>();

        final class PersonViewHolder extends RecyclerView.ViewHolder {

            TextView name;
            TextView lastName;
            TextView birthday;

            PersonViewHolder(View itemView) {
                super(itemView);
                name = itemView.findViewById(R.id.name);
                lastName = itemView.findViewById(R.id.lastName);
                birthday = itemView.findViewById(R.id.birthday);

            }

            void bind(RVWorkers person) {
                name.setText(person.name);
                lastName.setText(person.lastName);
                birthday.setText(person.birthday);
                itemView.setOnClickListener(v -> liveData.postValue(person));
            }
        }

        private List<RVWorkers> persons;

    RVAdapterWorkers(List<RVWorkers> persons) {
            this.persons = persons;
        }

        @Override
        public com.a65apps_tests.RVAdapterWorkers.PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_workers, viewGroup, false);
            com.a65apps_tests.RVAdapterWorkers.PersonViewHolder pvh = new com.a65apps_tests.RVAdapterWorkers.PersonViewHolder(itemView);
            return pvh;
        }

        @Override
        public void onBindViewHolder(com.a65apps_tests.RVAdapterWorkers.PersonViewHolder personViewHolder, int i) {
            personViewHolder.bind(persons.get(i));
        }

        @Override
        public int getItemCount() {
            return persons.size();
        }
}
