package com.a65apps_tests;

public interface OnBackButtonListener {

    boolean onBackPressed();

}
