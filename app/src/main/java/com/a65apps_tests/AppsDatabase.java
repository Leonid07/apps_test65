package com.a65apps_tests;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.a65apps_tests.model.SpecialtyUser;
import com.a65apps_tests.model.SpecialtyUserDao;
import com.a65apps_tests.model.User;
import com.a65apps_tests.model.UserDao;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {User.class, SpecialtyUser.class},version = 3,exportSchema = false)
public abstract class AppsDatabase extends RoomDatabase {

    private static final int NUMBER_OF_THREADS = 4;

    public abstract UserDao userDao();
    public abstract SpecialtyUserDao specialtyUserDao();

    private static volatile AppsDatabase INSTANCE;

    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static AppsDatabase getAppsDatabase(final Context context){
        if(INSTANCE == null) {
            synchronized (AppsDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppsDatabase.class,"AppsDatabase")
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }


    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);

            // If you want to keep data through app restarts,
            // comment out the following block
            databaseWriteExecutor.execute(() -> {

            });
        }
    };
}
