package com.a65apps_tests.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity
public class User {
    @PrimaryKey
    private long id;
    private String name;
    private String lastName;
    private String birthday;
    private String avatr_url;
    private String specialtyId;
    private String specialty_name;


    public User (@NonNull long id,  @NonNull String name, @NonNull String lastName,
                 String birthday, String avatr_url, @NonNull String specialtyId, @NonNull String specialty_name){  //@NonNull long specialtyUser_id,
        this.id= id;
        this.name = name;
        this.lastName = lastName;
        this.birthday = birthday;
        this.avatr_url = avatr_url;
        this.specialtyId = specialtyId;
        this.specialty_name =specialty_name;
    }

    @NonNull
    public long getId() {return this.id;}

    @NonNull
    public String getName(){return this.name;}

    @NonNull
    public String getLastName(){return this.lastName;}

    public String getBirthday(){return this.birthday;}

    public String getAvatr_url(){return this.avatr_url;}

    @NonNull
    public String getSpecialtyId(){return this.specialtyId;}

    @NonNull
    public String getSpecialty_name(){return  this.specialty_name;}

}
