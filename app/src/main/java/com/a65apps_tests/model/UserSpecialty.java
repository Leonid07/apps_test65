package com.a65apps_tests.model;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class UserSpecialty {

    @Embedded
    public User user;

    @Relation(parentColumn = "id", entityColumn ="user_id" )
    public List<SpecialtyUser> specialtyUsers;
// сделать таблицу юзеров главной и передавать ид юзера в специальности
}
